/* Sample C code for testing EncodeProCmd.c.
**
** Copyright 2015 Linx Technologies
**  155 Ort Lane
**  Merlin, OR, US 97532
**  www.linxtechnologies.com
**
** Disclaimer:
**  Linx Technologies makes not guarantee, representation, or warranty,
**  whether express, implied, or statutory, regarding the suitability of the
**  software for use in a specific application.
**  The company shall not, in any circumstances, be liable for special,
**  incidental, or consequential damages, for any reason whatsoever.
**
** License:
**  Permission is granted to use and modify this code, without royalty, for
**  any purpose, provided the copyright statement, disclaimer, and license
**  are included.
*/

#include <stdio.h>
#include <string.h>
#include "EncodeProCmd.h"

/* Generate length and address of byte array */
#define SLV(a)  (sizeof a)-1, a

/* Function:    testHumProRead
** Description: This function test HumProRead results against the
**              expected results.
**              An error messge is printed on failure.
*/
void testHumProRead(
    unsigned char reg,      /* register number to read, 0..0xff */
    unsigned char lcmd,     /* expected encoded length */
    unsigned char *ecmd     /* in: expected encoded command */
) {
    unsigned char ec[256];
    unsigned char el;

    el = HumProRead(ec, reg);
    if (el != lcmd) printf("Fail:HumProRead gave %d, expected %d\n", el, lcmd);
    else if (memcmp(ec, ecmd, el) != 0) {
        printf ("Fail: HumProRead gave wrong encoding\n");
    } else {
        printf ("Pass: HumProRead\n");
    }
}

/* Function:    testHumProWrite
** Description: This function test HumProWrite results against the
**              expected results.
**              An error messge is printed on failure.
*/
void testHumProWrite(
    unsigned char reg,      /* register number to write */
    unsigned char val,      /* value to write */
    unsigned char lcmd,     /* expected encoded length */
    unsigned char *ecmd     /* in: expected encoded command */
) {
    unsigned char ec[256];
    unsigned char el;

    el = HumProWrite(ec, reg, val);
    if (el != lcmd) printf("Fail:HumProWrite gave %d, expected %d\n", el, lcmd);
    else if (memcmp(ec, ecmd, el) != 0) {
        printf ("Fail: HumProWrite gave wrong encoding\n");
    } else {
        printf ("Pass: HumProWrite\n");
    }
}

/* Function:    testHumProCommand
** Description: This function test HumProCommand results against the
**              expected results.
**              An error messge is printed on failure.
*/
void testHumProCommand(
    unsigned char lin,      /* length of in */
    unsigned char *in,      /* in: raw bytes in command */
    unsigned char lcmd,     /* expected encoded length */
    unsigned char *ecmd     /* in: expected encoded command */
) {
    unsigned char ec[256];
    unsigned char el;

    el = HumProCommand(ec, in, lin);
    if (el != lcmd) printf("Fail:HumProCommand gave %d, expected %d\n", el, lcmd);
    else if (memcmp(ec, ecmd, el) != 0) {
        printf ("Fail: HumProCommand gave wrong encoding\n");
    } else {
        printf ("Pass: HumProCommand\n");
    }
}

int main(void) {
    testHumProRead(0x00, SLV("\xff\x01\x80"));
    testHumProRead(0x60, SLV("\xff\x01\xe0"));
    testHumProRead(0x7e, SLV("\xff\x02\xfe\x7e"));
    testHumProRead(0x80, SLV("\xff\x01\x00"));

    testHumProWrite(0,    0,    SLV("\xff\x02\x00\x00"));
    testHumProWrite(1,    0xff, SLV("\xff\x03\x01\xfe\x7f"));
    testHumProWrite(0xc9, 0xfe, SLV("\xff\x03\xc9\xfe\x7e"));

    testHumProCommand(SLV("\x01"), SLV("\xff\x01\x01"));
    testHumProCommand(SLV("\xef"), SLV("\xff\x01\xef"));
    testHumProCommand(SLV("\xf0"), SLV("\xff\x02\xfe\x70"));
    testHumProCommand(SLV("\xc7\x12\x01\xff\xff\xff\xff\xff\xff\xff\xff"
        "\xff\xff\xff\xff\xff\xff\xff\xff"),
        SLV("\xff\x23\xc7\x12\x01"
        "\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f"
        "\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f\xfe\x7f"));
    printf("Done\n");
}
